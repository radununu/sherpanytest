//
//  Album+CoreDataClass.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Album)
public class Album: NSManagedObject {

    convenience init(model: AlbumModel, insertIntoManagedObjectContext context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: "Album", in: context) ?? NSEntityDescription()
        self.init(entity: entity, insertInto: context)
        self.id = Int16(model.id)
        self.userId = Int16(model.userId)
        self.title = model.title
    }
}
