//
//  Post+CoreDataClass.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Post)
public class Post: NSManagedObject {

    convenience init(model: PostModel, insertIntoManagedObjectContext context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: "Post", in: context) ?? NSEntityDescription()
        self.init(entity: entity, insertInto: context)
        self.id = Int16(model.id)
        self.body = model.body
        self.userId = Int16(model.userId)
        self.title = model.title
    }
}
