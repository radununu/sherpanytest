//
//  Photo+CoreDataClass.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//
//

import CoreData

@objc(Photo)
public class Photo: NSManagedObject {
    convenience init(model: PhotoModel, insertIntoManagedObjectContext context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: "Photo", in: context) ?? NSEntityDescription()
        self.init(entity: entity, insertInto: context)
        self.id = Int16(model.id)
        self.albumId = Int16(model.albumId)
        self.thumbnailUrl = model.thumbnailUrl
        self.url = model.url
        self.title = model.title
    }
}
