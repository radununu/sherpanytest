//
//  User+CoreDataClass.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {

    convenience init(model: UserModel, insertIntoManagedObjectContext context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context) ?? NSEntityDescription()
        self.init(entity: entity, insertInto: context)
        self.id = Int16(model.id)
        self.name = model.name
        self.email = model.email
    }
}
