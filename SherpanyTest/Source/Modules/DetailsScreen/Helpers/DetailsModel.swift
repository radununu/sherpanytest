//
//  DetailsModel.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import Foundation

struct DetailsModel {
    let album: Album
    let photos: [Photo]
    var collapsed = true
}
