//
//  AlbumHeaderView.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import UIKit

class AlbumHeaderView: UICollectionReusableView {
    @IBOutlet weak var title: UILabel!
    var collapseClosure: (() -> Void)?
    
    static let identifier = "AlbumHeaderViewIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderColor = UIColor.darkGray.cgColor
        layer.borderWidth = 1
    }
    
    @IBAction func collapseAction() {
        collapseClosure?()
    }
    
}
