//
//  DetailsPostViewController.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import UIKit
import CoreData

class DetailsPostViewController: UIViewController {
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var postTitle: UILabel!
    @IBOutlet private weak var postDetails: UILabel!
    private var detailsModels = [DetailsModel]()
    private let cache = Cache<String, UIImage>()
    private let networkService = NetworkService()
    private let loadingIndicatorView = UIActivityIndicatorView()
    var post: Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchFromDataBase()
    }
    
    func setupUI() {
        postTitle.text = post?.title
        postDetails.text = post?.body
        loadingIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loadingIndicatorView)
        view.addConstraints([
            loadingIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        loadingIndicatorView.startAnimating()
    }
    
}

extension DetailsPostViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        detailsModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        detailsModels[section].collapsed ? 0 : detailsModels[section].photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as? PhotoCollectionViewCell else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath)
        }
        
        loadImage(for: cell.imageView, with: detailsModels[indexPath.section].photos[indexPath.row].url)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: AlbumHeaderView.identifier, for: indexPath) as? AlbumHeaderView else {
            return UICollectionReusableView()
        }
        
        sectionHeader.title.text = detailsModels[indexPath.section].album.title
        sectionHeader.collapseClosure = { [weak self] in
            self?.detailsModels[indexPath.section].collapsed = !(self?.detailsModels[indexPath.section].collapsed ?? true)
            self?.collectionView.reloadSections(IndexSet(arrayLiteral: indexPath.section))
        }
        return sectionHeader
    }
    
}

extension DetailsPostViewController: UICollectionViewDelegateFlowLayout {
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = UIDevice.current.userInterfaceIdiom == .pad ? 170 : 130

        return CGSize(width: size, height: size)
    }
    
}

private extension DetailsPostViewController {
        
    func fetchFromDataBase() {
        guard let post = post else { return }
        
        let context = CoreDataStack.shared.managedObjectContext
        let fetchAlbums: NSFetchRequest<Album> = Album.fetchRequest()
        fetchAlbums.predicate = NSPredicate(format: "userId == %i", post.userId)
        guard let albums = try? context.fetch(fetchAlbums) else { return }
        
        detailsModels = albums.compactMap { album in
            let fetchPhotos: NSFetchRequest<Photo> = Photo.fetchRequest()
            fetchPhotos.predicate = NSPredicate(format: "albumId == %i", album.id)
            guard let photos = try? context.fetch(fetchPhotos) else { return DetailsModel(album: album, photos: []) }
            return DetailsModel(album: album, photos: photos)
        }
        collectionView.reloadData()
        loadingIndicatorView.startAnimating()
        loadingIndicatorView.removeFromSuperview()
    }
    
    func loadImage(for imageView: UIImageView, with urlString: String?) {
        guard let urlString = urlString else { return }
        if let cached = cache[urlString] {
            DispatchQueue.main.async {
                imageView.image = cached
            }
        } else {
            networkService.downloadImage(with: urlString) { [weak self] result in
                switch result {
                case .success(let image):
                    self?.cache[urlString] = image
                    DispatchQueue.main.async {
                        imageView.image = image
                    }
                case .failure(_): break
                }
            }
        }
    }
}

