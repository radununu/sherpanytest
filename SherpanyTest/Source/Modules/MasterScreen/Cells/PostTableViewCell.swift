//
//  PostTableViewCell.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    static let cellIdentifier = "PostTableViewCellIdentifier"
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var userEmail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
