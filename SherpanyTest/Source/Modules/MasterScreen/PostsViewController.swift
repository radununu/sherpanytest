//
//  ViewController.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import UIKit
import CoreData

class PostsViewController: UITableViewController{
    private var dataList = [(post: Post, user: User)]()
    private var filtered = [(post: Post, user: User)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Challenge Accepted!"
        fetchFromDataBase()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    // MARK: - Table View
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.cellIdentifier) as? PostTableViewCell else {
            return UITableViewCell()
        }
        
        cell.title.text = filtered[indexPath.row].0.title
        cell.userEmail.text = filtered[indexPath.row].1.email
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showDetailView(for: filtered[indexPath.row].0)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            dataList.remove(at: indexPath.row)
            filtered.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
}

extension PostsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            filtered = dataList
            tableView.reloadData()
            return
        }
        filtered = dataList.filter {
            $0.post.title?.lowercased().contains(searchText.lowercased()) ?? false
        }
        tableView.reloadData()
    }
}

private extension PostsViewController {
    func fetchFromDataBase() {
        let context = CoreDataStack.shared.managedObjectContext
        let fetchPosts: NSFetchRequest<Post> = Post.fetchRequest()
        let fetchUsers: NSFetchRequest<User> = User.fetchRequest()
        guard let posts = try? context.fetch(fetchPosts), let users = try? context.fetch(fetchUsers) else { return }
        
        self.dataList = posts.compactMap({ post -> (Post, User) in
            return (post, users.first(where: { $0.id == post.userId })!)
        })
        filtered = dataList
    }
    
    func showDetailView(for post: Post) {
        guard let splitVC = splitViewController else { return }
        let storyboard = UIStoryboard(name: "DetailsPostViewController", bundle: .main)
        guard let detailsVC = storyboard.instantiateInitialViewController() as? DetailsPostViewController else { return }
        detailsVC.post = post
        let detailsNavigation = UINavigationController(rootViewController: detailsVC)
        splitVC.showDetailViewController(detailsNavigation, sender: self)
    }
}

