//
//  NetworkService.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import UIKit

protocol NetworkServiceProtocol {
    func request<T: Decodable>(with: Endpoint, responseType: T.Type, handler: @escaping (Result<T>) -> Void)
}

struct NetworkService: NetworkServiceProtocol {
    func request<T: Decodable>(with endpoint: Endpoint,
                               responseType: T.Type,
                               handler: @escaping (Result<T>) -> Void) {
        
        guard let url = endpoint.url else {
            return handler(.failure(NetworkError.invalidURL))
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard error == nil else {
                handler(.failure(error))
                return
            }
            
            guard let data = data else {
                handler(.failure(NetworkError.emptyResponse))
                return
            }
            
            guard let result = try? JSONDecoder().decode(responseType, from: data) else {
                handler(.failure(NetworkError.unableToSerialize))
                return
            }
            
            handler(.success(result))
        }.resume()
    }
    
    func downloadImage(with urlString: String,
                       handler: @escaping (Result<UIImage>) -> Void) {
        
        guard let url = URL(string: urlString) else {
            return handler(.failure(NetworkError.invalidURL))
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard error == nil else {
                handler(.failure(error))
                return
            }
            
            guard let data = data else {
                handler(.failure(NetworkError.emptyResponse))
                return
            }
            
            guard let result = UIImage(data: data) else {
                handler(.failure(NetworkError.unableToSerialize))
                return
            }
            
            handler(.success(result))
        }.resume()
    }

}
