//
//  PostModel.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import Foundation

public struct PostModel: Codable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}
