//
//  UserModel.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import Foundation

struct UserModel: Codable {
    let email: String
    let id: Int
    let name: String
}
