//
//  Endpoints.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import Foundation

protocol Endpoint {
    var url: URL? { get }
}

enum SherpanyEndpoints: Endpoint, CaseIterable {
    case getPosts, getUsers, getAlbums, getPhotos
    var url: URL? {
        switch self {
        case .getPosts:
            return URL(staticString: "http://jsonplaceholder.typicode.com/posts/")
        case .getUsers:
            return URL(staticString: "http://jsonplaceholder.typicode.com/users")
        case .getAlbums:
            return URL(staticString: "http://jsonplaceholder.typicode.com/albums")
        case .getPhotos:
            return URL(staticString: "http://jsonplaceholder.typicode.com/photos")
        }
    }
}

extension URL {
    init(staticString string: StaticString) {
        guard let url = URL(string: "\(string)") else {
            preconditionFailure("Invalid static URL string: \(string)")
        }

        self = url
    }
}
