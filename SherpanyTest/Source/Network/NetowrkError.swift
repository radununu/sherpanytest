//
//  NetowrkError.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case unableToSerialize
    case emptyResponse
}
