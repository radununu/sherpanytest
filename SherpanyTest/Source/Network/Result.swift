//
//  Result.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error?)
}
