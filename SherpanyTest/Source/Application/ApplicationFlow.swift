//
//  ApplicationFlow.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import UIKit

class ApplicationFlow {
    
    func setupRoot(in window: UIWindow?) {
        let splitViewController = UISplitViewController()
        splitViewController.preferredDisplayMode = .allVisible
        splitViewController.preferredPrimaryColumnWidthFraction = 1/3
        splitViewController.delegate = self
        splitViewController.viewControllers = [masterView(), detailsView()]
        
        window?.rootViewController = splitViewController
        window?.makeKeyAndVisible()
    }
    
    private func masterView() -> UIViewController {
        let storyboard = UIStoryboard(name: "PostsViewController", bundle: .main)
        guard let viewController = storyboard.instantiateInitialViewController() else { return UIViewController() }
        return UINavigationController(rootViewController: viewController)
    }
    
    private func detailsView() -> UIViewController {
        let storyboard = UIStoryboard(name: "DetailsPostViewController", bundle: .main)
        guard let viewController = storyboard.instantiateInitialViewController() else { return UIViewController() }
        return UINavigationController(rootViewController: viewController)
    }
    
}

extension ApplicationFlow: UISplitViewControllerDelegate {
    func splitViewController(
              _ splitViewController: UISplitViewController,
              collapseSecondary secondaryViewController: UIViewController,
              onto primaryViewController: UIViewController) -> Bool {
         return true
     }
}
