//
//  DataFetcher.swift
//  SherpanyTest
//
//  Created by Radu Nunu on 09.01.20.
//  Copyright © 2020 Nunu Radu. All rights reserved.
//

import Foundation

struct DataFetcher {
    static var shared = DataFetcher()
    let networkService = NetworkService()
    
    func fetchData() {
        let context = CoreDataStack.shared.managedObjectContext
        let group = DispatchGroup()

        group.enter()
        fetchPosts { posts in
            _ = posts.map { Post(model: $0, insertIntoManagedObjectContext: context) }
            group.leave()
        }
        
        group.enter()
        fetchUsers { users in
            _ = users.map { User(model: $0, insertIntoManagedObjectContext: context) }
            group.leave()
        }
        
        group.enter()
        fetchAlbums { albums in
            _ = albums.map { Album(model: $0, insertIntoManagedObjectContext: context) }
            group.leave()
        }
        
        group.enter()
        fetchPhotos { photos in
            _ = photos.map { Photo(model: $0, insertIntoManagedObjectContext: context) }
            group.leave()
        }
        
        group.wait()
        group.notify(queue: DispatchQueue.main) {
            CoreDataStack.shared.saveContext()
        }
        
    }
    
    private func fetchPosts(with completionHandler: @escaping ([PostModel]) -> Void) {
        networkService.request(with: SherpanyEndpoints.getPosts,
                               responseType: [PostModel].self) { (result) in
            switch result {
            case .success(let posts):
                completionHandler(posts)
            case .failure(_):
                completionHandler([])
            }
        }
    }
    
    private func fetchUsers(with completionHandler: @escaping ([UserModel]) -> Void) {
        networkService.request(with: SherpanyEndpoints.getUsers,
                               responseType: [UserModel].self) { (result) in
            switch result {
            case .success(let users):
                completionHandler(users)
            case .failure(_):
                completionHandler([])
            }
        }
    }
    
    private func fetchAlbums(with completionHandler: @escaping ([AlbumModel]) -> Void) {
        networkService.request(with: SherpanyEndpoints.getAlbums,
                               responseType: [AlbumModel].self) { (result) in
            switch result {
            case .success(let albums):
                completionHandler(albums)
            case .failure(_):
                completionHandler([])
            }
        }
    }
    
    private func fetchPhotos(with completionHandler: @escaping ([PhotoModel]) -> Void) {
        networkService.request(with: SherpanyEndpoints.getPhotos,
                               responseType: [PhotoModel].self) { (result) in
            switch result {
            case .success(let photos):
                completionHandler(photos)
            case .failure(_):
                completionHandler([])
            }
        }
    }
        
}
